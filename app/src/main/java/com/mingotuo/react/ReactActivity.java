package com.mingotuo.react;

import android.os.Bundle;
import com.facebook.react.ReactRootView;

/**
 * Created by mingotuo on 2017/5/26.
 */

public class ReactActivity extends BaseActivity {

  private ReactRootView mReactRootView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mReactRootView = new ReactRootView(this);

    // 注意这里的mingoT必须对应“index.android.js”中的
    // “AppRegistry.registerComponent()”的第一个参数
    mReactRootView.startReactApplication(mReactInstanceManager, "mingoT", null);

    setContentView(mReactRootView);
  }
}
